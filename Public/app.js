document.addEventListener('DOMContentLoaded', function() {
    $('button[data-task-id]').click(function(event) {
        let task = $(event.target).closest('tr').find('td:nth-child(3)').text();
        $('#modalEdit form input[name="task"]').val(task);
        $('#modalEdit form input[name="id"]').val($(event.target).data('task-id'));
        $('#modalEdit').modal({ show: true });
    });

    $('#modalEdit form').submit(function(event) {
        event.preventDefault();
        $('#modalEdit form button[type="submit"]').text('Загрузка');
        let data = {
            id: $('#modalEdit form input[name="id"]').val(),
            task: $('#modalEdit form input[name="task"]').val()
        };
        $.post('index.php?route=admin/edit', data)
            .then(function(resp) {
                if(resp.redirect) window.location = resp.redirect;
                else if(resp.status = 'failed' && resp.errors.legnth > 0){
                    resp.errors.foreach(function (error) {
                        $('#modalEdit form input').prepend(error);
                    });
                }
                $('#modalEdit form button[type="submit"]').text('Сохранить');
            }).catch(function(error) {
                $('#modalEdit form input').prepend(error);
                $('#modalEdit form button[type="submit"]').text('Сохранить');
            });
    });
});