<?php

class HomeController extends Controller {
    function __construct() {
        $this->view = new View();
        require_once('App/Model/Task.php');
        $this->model = new TaskModel();
    }

    public function validateSort($sort) {
        return in_array(
            $sort,
            [
                'id_DESC',
                'username_ASC',
                'username_DESC',
                'email_ASC',
                'email_DESC',
                'status_ASC',
                'status_DESC'
            ]
        );
    }

    public function index() {
        $data = [];
        $data['page_title'] = 'Главная';
        $data['content'] = 'Home';
        if(!empty($_SESSION['errors']) && count($_SESSION['errors']) > 0) {
            $data['errors'] = $_SESSION['errors'];
            unset($_SESSION['errors']);
        }
        if(!empty($_SESSION['success'])) {
            $data['success'] = $_SESSION['success'];
            unset($_SESSION['success']);
        }
        if(isset($_SESSION['authed'])) $data['authed'] = true;
        $filters = [];
        $filters['page'] = 1;
        if(isset($_GET['page'])) $filters['page'] = (int)strip_tags($_GET['page']);
        $filters['sort'] = 'id_DESC';
        $data['sort_default'] = 'index.php?route=home/index&' . http_build_query(array_merge($filters, array('sort' => 'id_DESC')));
        if(isset($_GET['sort']) && $this->validateSort(strip_tags($_GET['sort']))) $filters['sort'] = strip_tags($_GET['sort']);
        $data['sort'] = $filters['sort'];
        
        $data['username_sort_asc'] = 'index.php?route=home/index&' . http_build_query(array_merge($filters, array('sort' => 'username_ASC')));
        $data['username_sort_desc'] = 'index.php?route=home/index&' . http_build_query(array_merge($filters, array('sort' => 'username_DESC')));
        $data['email_sort_asc'] = 'index.php?route=home/index&' . http_build_query(array_merge($filters, array('sort' => 'email_ASC')));
        $data['email_sort_desc'] = 'index.php?route=home/index&' . http_build_query(array_merge($filters, array('sort' => 'email_DESC')));
        
        $data['status_sort_asc'] = 'index.php?route=home/index&' . http_build_query(array_merge($filters, array('sort' => 'status_ASC')));
        $data['status_sort_desc'] = 'index.php?route=home/index&' . http_build_query(array_merge($filters, array('sort' => 'status_DESC')));
        
        $data['complete_url'] = 'index.php?route=home/complete&' . http_build_query($filters);
        
        $data['tasks'] = $this->model->getTasks($filters);
        $total_tasks = $this->model->getTotalTasks();
        $total_pages = ceil($total_tasks / $this->model->limit);
        $data['total_pages'] = $total_pages;
        if($total_pages > 1) {
            $data['pagination'] = [];
            for($i=1; $i <= $total_pages; $i++) $data['pagination'][$i] = 'index.php?route=home/index&' . http_build_query(array_merge($filters, ['page' => $i]));
            $data['current_page'] = $filters['page'];
        }
        $this->view->render($data);
    }
    public function create() {
        if(!isset($_POST['create_task'])) {
            header('Location: index.php?route=home/index');
            return;
        }
        $errors = [];
        $data = [];
        if(empty(strip_tags($_POST['username']))) {
            $errors[] = "Введите имя пользователя!";
        } else {
            $data['username'] = addslashes(strip_tags($_POST['username']));
        }
        if(empty(strip_tags($_POST['email']))) {
            $errors[] = "Введите email!";
        } else if(!filter_var(strip_tags($_POST['email']), FILTER_VALIDATE_EMAIL)) {
            $errors[] = "Некорректный адрес email!";
        } else {
            $data['email'] = addslashes(strip_tags($_POST['email']));
        }
        if(empty(strip_tags($_POST['task']))) {
            $errors[] = "Введите задачу!";
        } else {
            $data['task'] = addslashes(strip_tags($_POST['task']));
        }
        if(count($errors) > 0) {
            $_SESSION['errors'] = $errors;
            header('Location: index.php?route=home/index');
            return;
        }
        $this->model->create($data);
        $_SESSION['success'] = 'Задача создана!';
        header('Location: index.php?route=home/index');
    }

    public function complete() {
        if(!empty($_POST['complete'])) {
            $task_id = (int)strip_tags($_POST['complete']);
            $res = $this->model->complete($task_id);
            if($res) $_SESSION['success'] = 'Задача завершена!';
        }
        header('Location: index.php?route=home/index');
    }
}