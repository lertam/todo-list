<?php

class AdminController extends Controller {
    public function __construct(){
        $this->view = new View();
        require_once('App/Model/Task.php');
        $this->model = new TaskModel();
    }
    public function login() {
        if(isset($_SESSION['authed'])) {
            header('Location: index.php?route=home/index');
        }
        $data = [];
        $data['page_title'] = 'Вход';
        $data['content'] = 'Auth';
        $errors = [];
        if(isset($_POST['login'])) {
            if($_POST['username'] == 'admin' && $_POST['password'] == '123') {
                $_SESSION['authed'] = true;
                $_SESSION['success'] = 'Вы успешно авторизировались!';
                header('Location: index.php?route=home/index');
            } else {
                $errors[] = 'Проверьте введенные данные!';
            }
        }
        if(count($errors) > 0) $data['errors'] = $errors;
        if(isset($_SESSION['errors'])) {
            $data['errors'] = array_merge($_SESSION['errors'], $data['errors']);
            unset($_SESSION['errors']);
        }
        $this->view->render($data);
    }

    public function logout() {
        if(isset($_SESSION['authed'])) {
            unset($_SESSION['authed']);
            $_SESSION['success'] = 'Вы вышли из аккаунта!';
        }
        header('Location: index.php?route=home/index');
    }

    public function edit() {
        if(strtoupper($_SERVER['REQUEST_METHOD']) != 'POST') {
            header('Location: index.php?route=home/index');
            return;
        }
        if(!isset($_SESSION['authed'])) {
            if(isset($_SESSION['errors'])) $_SESSION['errors'] = array_merge($_SESSION['errors'], ['Необходимо авторизироваться!']);
            else {
                $_SESSION['errors'] = [];
                $_SESSION['errors'] = array_merge($_SESSION['errors'], ['Необходимо авторизироваться!']);
            }
            // header('Location: index.php?route=home/index');
            header('Content-Type: application/json');
            echo json_encode(['redirect' => 'index.php?route=home/index']);
            return;
        }
        $errors = [];
        $data=[];
        if(!isset($_POST['id'])) {
            $errors[] = 'Укажите ID';
        } else {
            $data['id'] = addslashes(strip_tags($_POST['id']));
        }
        if(empty(strip_tags($_POST['task']))) {
            $errors[] = "Введите задачу!";
        } else {
            $data['task'] = addslashes(strip_tags($_POST['task']));
        }
        if(count($errors) > 0) {
            $_SESSION['errors'] = $errors;
            // header('Location: index.php?route=home/index');
            header('Content-Type: application/json');
            echo json_encode([
                'status' => 'failed',
                'errors' => $errors
            ]);
            return;
        }
        $result = $this->model->edit($data);
        if(!$result) $_SESSION['errors'] = array_merge($_SESSION['errors'], ['Что-то пошло не так!']);
        else $_SESSION['success'] = 'Задача отредактирована!';
        header('Content-Type: application/json');
        echo json_encode([
            'redirect' => 'index.php?route=home/index'
        ]);
    }
}