<div class="col-12 col-sm-6 col-md-3 m-auto">
    <?php if($errors): ?>
        <?php foreach($errors as $error): ?>
            <div class="alert alert-danger" role="alert">
                <?= $error ?>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
    <form method="POST">
        <div class="form-group">
            <label for="username_input">Логин</label>
            <input type="text" class="form-control" name="username" id="username_input"">
        </div>
        <div class="form-group">
            <label for="password_input">Пароль</label>
            <input type="password" class="form-control" name="password" id="password_input">
        </div>
        <button type="submit" class="btn btn-primary" name="login">Войти</button>
    </form>
</div>