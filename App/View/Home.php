<div class="container">
    <h1>Список задач</h1>
    <?php if($success): ?>
        <div class="alert alert-success" role="alert">
            <?= $success ?>
        </div>
    <?php endif; ?>
    <?php if($errors): ?>
        <?php foreach($errors as $error): ?>
            <div class="alert alert-danger" role="alert">
                <?= $error ?>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>
    <form class="form-inline" method="POST" action="index.php?route=home/create">
        <div class="mb-2 mr-sm-2">
            <input class="form-control" type="text" placeholder="Имя пользователя" name="username" />
        </div>
        <div class="mb-2 mr-sm-2">
            <input class="form-control" type="email" placeholder="E-mail" name="email" />
        </div>
        <div class="mb-2 mr-sm-2">
            <input class="form-control" type="text" placeholder="Задача" name="task" />
        </div>
        <div class="mb-2 mr-sm-2">
            <button class="btn btn-primary" type="submit" name="create_task">Создать</button>
        </div>
    </form>
    <?php if(empty($tasks) or count($tasks) == 0):?>
        <h3>Задач не найдено</h3>
    <?php else : ?>
        <table class="table table-stripped">
            <thead>
                <th>
                    <?php if($sort == 'username_ASC'): ?>
                        <a href="<?= $username_sort_desc ?>">Пользователь &uArr;;</a>
                    <?php elseif($sort == 'username_DESC'): ?>
                        <a href="<?= $sort_default ?>">Пользователь &dArr;</a>
                    <?php else: ?>
                        <a href="<?= $username_sort_asc ?>">Пользователь</a>
                    <?php endif; ?>
                </th>
                <th>
                    <?php if($sort == 'email_ASC'): ?>
                        <a href="<?= $email_sort_desc ?>">E-mail &uArr;</a>
                    <?php elseif($sort == 'email_DESC'): ?>
                        <a href="<?= $sort_default ?>">E-mail &dArr;</a>
                    <?php else: ?>
                        <a href="<?= $email_sort_asc ?>">E-mail</a>
                    <?php endif; ?>
                </th>
                <th>Задача</th>
                <th><?php if($sort == 'status_ASC'): ?>
                        <a href="<?= $status_sort_desc ?>">Статус &uArr;</a>
                    <?php elseif($sort == 'status_DESC'): ?>
                        <a href="<?= $sort_default ?>">Статус &dArr;</a>
                    <?php else: ?>
                        <a href="<?= $status_sort_asc ?>">Статус</a>
                    <?php endif; ?></th>
                <?php if($authed): ?>
                    <th>Действия</th>
                <?php endif; ?>
            </thead>
            <tbody>
                <?php foreach($tasks as $task): ?>
                    <tr>
                        <td><?= $task['username'] ?></td>
                        <td><?= $task['email'] ?></td>
                        <td><?= $task['task'] ?></td>
                        <td>
                            <?php if($task['status'] == 'new'): ?>
                                Новая
                            <?php else : ?>
                                Выполнена
                            <?php endif; ?>
                            <?php if($task['edited_by_admin']): ?>
                                <br />Отредактировано администратором
                            <?php endif; ?>
                        </td>
                        <?php if($authed): ?>
                            <td class="d-flex justify-content-around">
                                <form action="<?= $complete_url ?>" method="POST">
                                    <button type="submit" class="btn btn-primary" name="complete" value="<?= $task['id'] ?>">✔</button>
                                </form>
                                <button data-task-id="<?= $task['id'] ?>" class="btn btn-primary">🖉</button>
                            </td>
                        <?php endif; ?>
                    </tr>    
                <?php endforeach; ?>
            </tbody>
        </table>
        <span>Всего страниц -  <?= $total_pages ?></span>
        <?php if($pagination): ?>
            <nav>
                <ul class="pagination">
                    <?php foreach($pagination as $page => $link): ?>
                        <li class="page-item <?php echo ($page == $current_page) ? 'active' : '' ?>"><a class="page-link" href="<?= $link ?>"><?= $page ?></a></li>
                    <?php endforeach; ?>
                </ul>
            </nav>
        <?php endif; ?>
        <div class="modal fade" id="modalEdit" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="index.php?route=admin/edit" method="POST">
                        <div class="modal-header">
                            <h5 class="modal-title">Редактировать задачу</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span>&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" name="id" />
                            <input type="text" name="task" value="" required />
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>