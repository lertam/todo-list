<div class="container">
    <h1>Редактирование задачи</h1>
    <form class="form" method="POST">
        <div class="form-group">
            <label for="id_input">ID</label>
            <input id="id_input" class="form-control" name="id" value="<?= $data['task']['id'] ?>" />
        </div>
        <div class="form-group">
            <label for="username_input">Логин</label>
            <input id="username_input" class="form-control" name="username" value="<?= $data['task']['username'] ?>" />
        </div>
        <div class="form-group">
            <label for="email_input">E-mail</label>
            <input id="email_input" class="form-control" name="email" value="<?= $data['task']['email'] ?>"/>
        </div>
        <div class="form-group">
            <label for="task_input">Задача</label>
            <input id="task_input" class="form-control" name="task" value="<?= $data['task']['task'] ?>"/>
        </div>
        <div class="form-group">
            <label for="status_input">Статус</label>
            <select class="form-control" name="status" id="status_input">
                <option value="new" <?php echo $data['task']['status'] == 'new' ? 'selected':''; ?>>Новая</option>
                <option value="completed" <?php echo $data['task']['status'] == 'completed' ? 'selected':''; ?>>Выполнена</option>
            </select
        </div>
        <button type="submit" class="btn btn-primary" name="edit">Сохранить</button>
    </form>
</div>