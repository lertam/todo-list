<?php

require_once('App/config.php');

spl_autoload_register(function ($name) {
    require_once('App/Core/' . $name . '.php');
});

