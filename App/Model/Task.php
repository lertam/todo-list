<?php

class TaskModel extends Model {
    public $limit = 3;
    public function getTasks($filters = []) {
        $sql = "SELECT * FROM `tasks` ";
        if(isset($filters['sort'])) {
            $parts = explode('_', $filters['sort']);
            $column = $parts[0];
            $direction = $parts[1];
            $sql .= " ORDER BY `" . $column . '` ' . $direction;
        }
        $sql .= " LIMIT " . $this->limit;
        if(!empty($filters['page'])) {
            $sql .= " OFFSET " . ($filters['page'] - 1) * $this->limit;
        }
        $result = $this->db->query($sql);
        if($result)return $result->fetch_all(MYSQLI_ASSOC);
        else throw new Exception($this->db->error);
    }

    public function getTotalTasks() {
        $sql = "SELECT COUNT(`id`) AS count FROM `tasks`;";
        $res = $this->db->query($sql);
        if($res) return $res->fetch_assoc()['count'];
        else throw new Exception($this->db->error);
    }

    public function create($task) {
        $result = $this->db->query("INSERT INTO `tasks` (`username`, `email`, `task`) VALUES ('" . $task['username'] . "', '" . $task['email'] . "', '" . $task['task'] . "')");
        if($this->db->errno) throw new Exception($this->db->error);
        else return $result;
    }

    public function complete($task_id) {
        $sql = "UPDATE `tasks` SET `status` = 'completed' WHERE `id` = " . $task_id;
        return $this->db->query($sql);
    }

    public function getTask($task_id) {
        $task = $this->db->query('SELECT * FROM `tasks` WHERE `id`=' . $task_id);
        if($task) return $task->fetch_assoc();
        return $task;
    }

    public function edit($data) {
        if(!isset($data['id'])) throw new Exception('Not Found');
        $task = $this->getTask($data['id']);
        if($task) {
            //task exists
            $by_admin = 0;
            if($task['task'] != $data['task']) $by_admin = 1;
            $result = $this->db->query("UPDATE `tasks` SET `task` = '" . $data['task'] . "', edited_by_admin='" . $by_admin . "' WHERE `id`='" . $data['id'] . "';");
            return $result;
        } else {
            return false;
        }
    }
}