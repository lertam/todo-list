<?php

class Model {
    public $db = null;

    function __construct() {
        $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        if ($mysqli->connect_error) {
            throw new Exception('Connect Error (' . $mysqli->connect_errno . ') ' . $mysqli->connect_error);
        } else {
            $this->db = $mysqli;
        }
        $this->createTables();
    }

    private function createTables() {
    }
}