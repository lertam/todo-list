<?php

class Router {
    public static function run() {
        $controller_name = 'Home';
        $action_name = 'Index';
        // route like %controller%/%action%
        if(!empty($_REQUEST['route'])) {
            $parts = explode('/', $_REQUEST['route']);
            $controller_name = ucfirst($parts[0]);
            if(count($parts) > 1) {
                $action_name = $parts[1];
            }
        }

        $controller_path = 'App/Controller/' . $controller_name . '.php';
        if(file_exists($controller_path)) {
            require($controller_path);
        } else {
            throw new Exception("$controller_name not found");
        }

        // Add prefixes
        $controller_name = $controller_name . 'Controller';
        $controller = new $controller_name();

        if(method_exists($controller, $action_name)) {
			$controller->$action_name();
		} else {
            throw new Exception("Action $controller_name@$action_name not found");
        }
    }
}