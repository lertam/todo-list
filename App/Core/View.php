<?php

class View {
    public $template = 'Layout';

    public function render($data = null) {
        if(is_array($data)) {
            extract($data);
        }
        include_once('App/View/' . $this->template . '.php');
    }
}